#include <iostream>
#include "opencv2\core\core.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\highgui\highgui.hpp"

using namespace cv;
using namespace std;

int main() {
	Mat image = imread("ORANGE.jpg", 0);
	if (image.empty()) return -1;
	namedWindow("img", WINDOW_AUTOSIZE);
	imshow("img", image);

	int th = 60;
	Mat threshold(image.rows, image.cols, CV_8U);
	for (int i = 0; i < threshold.rows; i++)
	{
		for (int j = 0; j < threshold.cols; j++)
		{
			threshold.at<uchar>(i, j) = (uchar)(image.at<uchar>(i, j) > th) ? 255 : 0;
		}
	}
	namedWindow("threshold", WINDOW_AUTOSIZE);
	imshow("threshold", threshold);

	Mat inverseThreshold(image.rows, image.cols, CV_8U);
	for (int i = 0; i < threshold.rows; i++)
	{
		for (int j = 0; j < threshold.cols; j++)
		{
			inverseThreshold.at<uchar>(i, j) = (uchar)(image.at<uchar>(i, j) < th) ? 255 : 0;
		}
	}
	namedWindow("inverseThreshold", WINDOW_AUTOSIZE);
	imshow("inverseThreshold", inverseThreshold);

	waitKey(0);
	return 0;
}