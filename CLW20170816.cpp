#include <iostream>
#include "opencv2\core\core.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\highgui\highgui.hpp"

using namespace cv;
using namespace std;

Mat MakeMatColor(int width, int height, int tone);
int HistogramMaxValue(int* histogram);

int main() {
	int matSize = 512;
	Mat src = imread("Landscape.jpg", 0);
	if (src.empty()) return -1;
	/*namedWindow("img", WINDOW_KEEPRATIO);
	imshow("img", src);*/

	Mat hst(matSize, matSize, CV_8U);

	hst = MakeMatColor(matSize, matSize, 0);

	//rectangle(hst, Point(50, 400), Point(50, matSize), Scalar(255, 255, 255), 1, 8, 0);

	int histogram[256];
	int maxValue = 0;
	for (int i = 0; i < 257; i++) histogram[i] = 0;

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			int aux = (int)src.at<uchar>(i, j);
			histogram[aux]++;
		}
	}
	maxValue = HistogramMaxValue(histogram);
	for (int i = 0; i < 256; i++)
	{
		int height = (int)histogram[i] * matSize / maxValue;
		rectangle(hst, Point(i*2, matSize - height), Point(i*2+2, matSize), Scalar(255, 255, 255), 2, 8, 0);
	}

	namedWindow("Histogram", WINDOW_KEEPRATIO);
	imshow("Histogram", hst);
	waitKey(0);
	return 0;
}
int HistogramMaxValue(int* histogram) {
	int maxValue = 0;
	for (int i = 0; i < 256; i++)
	{
		if (histogram[i] > maxValue)maxValue = histogram[i];
	}
	return maxValue;
}
Mat MakeMatColor(int width, int height, int tone) {
	Mat aux(width, height, CV_8U);
	for (int i = 0; i < aux.rows; i++)
		for (int j = 0; j < aux.cols; j++)
			aux.at<char>(i, j) = tone;
	return aux;
}