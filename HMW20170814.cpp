#include <iostream>
#include "opencv2\core\core.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\highgui\highgui.hpp"

using namespace cv;
using namespace std;

Mat ChessBlock(int BlockSize);
int main() {
	/*Mat A22 = (Mat_<float>(3, 3) << 0, 1, 2, 3, 4, 5, 6, 7, 8);
	cout << "A22= " << endl << A22;*/
	Mat ej0(500, 500, CV_8U);
	for (int i = 0; i < ej0.rows; i++)
		for (int j = 0; j < ej0.cols; j++)
			ej0.at<char>(i, j) = (char)255;

	Mat ej3(500, 500, CV_8U);
	for (int i = 0; i < ej3.rows; i++)
		for (int j = 0; j < ej3.cols; j++)
			ej3.at<char>(i, j) = (i % 4 == 0 || i % 4 == 1) ? (char)0 : (char)255;

	Mat ej4(500, 500, CV_8U);
	for (int i = 0; i < ej4.rows; i++)
		for (int j = 0; j < ej4.cols; j++)
			ej4.at<char>(i, j) = (j % 4 == 0 || j % 4 == 1) ? (char)0 : (char)255;

	Mat ej5(500, 500, CV_8U);
	for (int i = 0; i < ej5.rows; i++)
		for (int j = 0; j < ej5.cols; j++)
			ej5.at<char>(i, j) = 255 - j / (2);

	Mat ej6(500, 500, CV_8U);
	for (int i = 0; i < ej6.rows; i++)
		for (int j = 0; j < ej6.cols; j++)
			ej6.at<char>(i, j) = (int)(i + j) / 4;


	int borderSize = 100;
	Mat ej7(500, 500, CV_8U);
	for (int i = 0; i < ej7.rows; i++) {
		for (int j = 0; j < ej7.cols; j++) {
			ej7.at<char>(i, j) = 0;
			if (i > borderSize && i < ej7.rows - borderSize &&j>borderSize&&j < ej7.cols - borderSize)
				ej7.at<char>(i, j) = 255;
			if (2 * borderSize < i && 2 * borderSize<j && ej7.rows - 2 * borderSize>i && ej7.cols - 2 * borderSize>j)
				ej7.at<char>(i, j) = 0;
		}
	}
	Mat ej8 = ChessBlock(100);
	Mat ej9 = ChessBlock(255);
	return 0;
}
Mat ChessBlock(int BlockSize) {
	bool main = false, sec = main;
	Mat aux(500, 500, CV_8U);
	for (int i = 0; i < aux.rows; i++) {
		sec = main;
		for (int j = 0; j < aux.cols; j++) {
			sec = (j % BlockSize == 0 && j > 0) ? !sec : sec;

			aux.at<char>(i, j) = (sec) ? 255 : 0;
		}
		main = (i % BlockSize == 0 && i > 0) ? !main : main;
	}
	return aux;
}