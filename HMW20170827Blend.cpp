#include <iostream>
#include "opencv2\core\core.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\highgui\highgui.hpp"

using namespace cv;
using namespace std;

Mat blendMaterial(Mat srcA, Mat srcB, float scrABelnd);
Mat GradientMap(Mat src);
int main() {

	char* source_windowA = "Source imageA";
	char* source_windowB = "Source imageB";
	char* Mixed_Window = "Mixed Image";
	char* gradient_Window = "Gradient Image";

	Mat F = imread("F.jpg", 0);
	Mat G = imread("G.jpg", 0);

	if (F.empty()) return -1;
	if (G.empty()) return -1;

	Mat H(F.rows, F.cols, CV_8U);
	Mat I(F.rows, F.cols, CV_8U);

	H = blendMaterial(F, G, 0.5f);
	I = blendMaterial(F, GradientMap(F), 0.5f);

	namedWindow(source_windowA, CV_WINDOW_AUTOSIZE);
	namedWindow(source_windowB, CV_WINDOW_AUTOSIZE);
	namedWindow(Mixed_Window, CV_WINDOW_AUTOSIZE);
	namedWindow(gradient_Window, CV_WINDOW_AUTOSIZE);

	imshow(source_windowA, F);
	imshow(source_windowB, G);
	imshow(Mixed_Window, H);
	imshow(gradient_Window, I);

	waitKey(0);
	return 0;
}

Mat blendMaterial(Mat srcA, Mat srcB, float srcABelnd)
{
	if (srcA.cols != srcB.cols || srcA.rows != srcB.rows) {
		return Mat();
	}

	Mat dst(srcA.rows, srcB.cols, CV_8U);

	if (srcABelnd < 0)srcABelnd = 0;
	if (srcABelnd > 1)srcABelnd = 1;

	float srcBBlend = 1 - srcABelnd;
	for (int i = 0; i < srcA.rows; i++)
	{
		for (int j = 0; j < srcA.cols; j++)
		{
			dst.at<uchar>(i, j) = ((srcABelnd*srcA.at<uchar>(i, j) + srcBBlend*srcB.at<uchar>(i, j)) / 2);

		}
	}
	return dst;
}

Mat GradientMap(Mat src)
{
	Mat gradient(src.rows, src.cols, CV_8U);

	for (int i = 0; i < gradient.rows; i++)
	{
		for (int j = 0; j < gradient.cols; j++)
		{
			gradient.at<uchar>(i, j) = 255-(j * 255 / gradient.cols);
		}
	}
	return gradient;
}
