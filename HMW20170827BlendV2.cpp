#include <iostream>
#include "opencv2\core\core.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\highgui\highgui.hpp"

using namespace cv;
using namespace std;

enum Direction { horizontal, vertical };
enum Type { addition, substract };

Mat blendMaterial(Mat srcA, Mat srcB, Direction dir);
Mat inverseMaterial(Mat src);
Mat blendMaterial(Mat srcA, Mat srcB, double srcABelnd, Type operation);
Mat GradientMap(Mat src, Direction dir);


int main() {

	char* source_windowA = "Source imageA";
	char* source_windowB = "Source imageB";
	char* Mixed_Window = "Mixed Image";
	char* gradient_Window = "Gradient Image";
	char* inverse_Window = "inverse Image";
	char* Add_Window = "Add Image";
	char* substract_Window = "substract Image";


	Mat F = imread("F.jpg", 0);
	Mat G = imread("G.jpg", 0);

	if (F.empty()) return -1;
	if (G.empty()) return -1;

	Mat addImages(F.rows, F.cols, CV_8U);
	Mat substractImages(F.rows, F.cols, CV_8U);
	Mat inverseImages(F.rows, F.cols, CV_8U);
	Mat blendImagesHorizontal(F.rows, F.cols, CV_8U);
	Mat blendImagesVertical(F.rows, F.cols, CV_8U);

	addImages = blendMaterial(F, G, 0.5, addition);
	substractImages = blendMaterial(F, G, 0.5, substract);
	inverseImages = inverseMaterial(G);
	blendImagesHorizontal = blendMaterial(F, G, horizontal);
	blendImagesVertical = blendMaterial(F, G, vertical);

	namedWindow(source_windowA, CV_WINDOW_AUTOSIZE);
	namedWindow(source_windowB, CV_WINDOW_AUTOSIZE);
	namedWindow(gradient_Window, CV_WINDOW_AUTOSIZE);
	namedWindow(inverse_Window, CV_WINDOW_AUTOSIZE);
	namedWindow(substract_Window, CV_WINDOW_AUTOSIZE);
	namedWindow(Add_Window, CV_WINDOW_AUTOSIZE);

	imshow(source_windowA, F);
	imshow(source_windowB, G);
	imshow(Add_Window, addImages);
	imshow(substract_Window, substractImages);
	imshow(inverse_Window, inverseImages);
	imshow(gradient_Window, blendImagesVertical);


	waitKey(0);
	return 0;
}
Mat blendMaterial(Mat srcA, Mat srcB, Direction dir) {
	if (srcA.cols != srcB.cols || srcA.rows != srcB.rows)
		return Mat();
	Mat dst(srcA.rows, srcA.cols, CV_8U);
	Mat gradient = GradientMap(dst, horizontal);
	for (int i = 0; i < srcA.rows; i++)
	{
		for (int j = 0; j < srcA.cols; j++)
		{
			float valueA = gradient.at<uchar>(i, j) / 255;
			float valueB = 1 - valueA;
			dst.at<uchar>(i, j) = ((valueA * srcA.at<uchar>(i, j) + valueB * srcB.at<uchar>(i, j)) / 2);
		}
	}
	return dst;
}

Mat inverseMaterial(Mat src) {
	Mat dst(src.rows, src.cols, CV_8U);
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			dst.at<uchar>(i, j) = 255 - src.at<uchar>(i, j);
		}
	}
	return dst;
}
Mat blendMaterial(Mat srcA, Mat srcB, double srcABelnd, Type operation)
{
	if (srcA.cols != srcB.cols || srcA.rows != srcB.rows) {
		return Mat();
	}

	Mat dst(srcA.rows, srcB.cols, CV_8U);

	if (srcABelnd < 0)srcABelnd = 0;
	if (srcABelnd > 1)srcABelnd = 1;

	float srcBBlend = 1 - srcABelnd;
	for (int i = 0; i < srcA.rows; i++)
	{
		for (int j = 0; j < srcA.cols; j++)
		{
			if (operation == addition) {
				int aux = ((srcABelnd*srcA.at<uchar>(i, j) + srcBBlend*srcB.at<uchar>(i, j)) / 2);
				dst.at<uchar>(i, j) = (aux > 255) ? 255 : aux;
			}
			else {
				int aux = ((srcABelnd*srcA.at<uchar>(i, j) - srcBBlend*srcB.at<uchar>(i, j)) / 2);
				dst.at<uchar>(i, j) = (aux < 0) ? 0 : aux;
			}

		}
	}
	return dst;
}

Mat GradientMap(Mat src, Direction dir)
{
	Mat gradient(src.rows, src.cols, CV_8U);
	switch (dir)
	{
	case horizontal:
		for (int i = 0; i < gradient.rows; i++)
		{
			for (int j = 0; j < gradient.cols; j++)
			{
				gradient.at<uchar>(i, j) = 255 - (j * 255 / gradient.cols);
			}
		}
		break;
	case vertical:
		for (int i = 0; i < gradient.rows; i++)
		{
			for (int j = 0; j < gradient.cols; j++)
			{
				gradient.at<uchar>(i, j) = 255 - (i * 255 / gradient.rows);
			}
		}
		break;
	}
	return gradient;
}
