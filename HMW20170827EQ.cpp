#include <iostream>
#include "opencv2\core\core.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\highgui\highgui.hpp"

using namespace cv;
using namespace std;
Mat EqualizeMaterial(Mat src);
int main() {
	char* source_window = "Source image";
	char* Equalized_window = "Equalized image";
	Mat src;
	src = imread("Wallpaper.jpg", CV_LOAD_IMAGE_GRAYSCALE);

	if (!src.data) {
		cout << "Image not found";
		return -1;
	}
	Mat dst(src.rows, src.cols, CV_8U);
	

	dst = EqualizeMaterial(src);

	namedWindow(source_window, WINDOW_NORMAL);
	imshow(source_window, src);

	namedWindow(Equalized_window, WINDOW_NORMAL);
	imshow(Equalized_window, dst);
	cout << "Code finished";
	waitKey(0);
	return 0;
}

Mat EqualizeMaterial(Mat src) {
	Mat dst(src.rows, src.cols, CV_8U);//Material a crear

	int hist[256] = {};//histograma original
	int cdf[256] = {};//histograma modificado

	for (int i = 0; i < src.rows; i++)
		for (int j = 0; j < src.cols; j++)
			hist[src.at<uchar>(i, j)]++;//Calcular el histograma SIN normalizar

	cdf[0] = hist[0];//necesario para que funcione
	for (int i = 1; i < 256; i++)
		cdf[i] = cdf[i - 1] + hist[i];//Ecuacion para sacar la probabilidad que toque ese tono de gris

	for (int i = 1; i < 256; i++)
		hist[i] = round((((float)cdf[i]) / (src.rows*src.cols)) * 254) + 1; // ecuacion para normalizar
	hist[0] = 0;//Valor necesario

	for (int i = 0; i < src.rows; i++) {
		for (int j = 0; j < src.cols; j++) {
			dst.at<uchar>(i, j) = hist[src.at<uchar>(i, j)];//asignacion de los valores al nuevo material
		}
	}
	return dst;//retorno del nuevo material
}
