#include <iostream>
#include "opencv2\core\core.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\highgui\highgui.hpp"

using namespace cv;
using namespace std;

enum Direction { horizontal, vertical };
enum Type { addition, substract };

Mat blendMaterial(Mat srcA, Mat srcB, Direction dir);
Mat inverseMaterial(Mat src);
Mat blendMaterial(Mat srcA, Mat srcB, double srcABelnd, Type operation);
Mat GradientMap(Mat src, Direction dir);
Mat Mask(Mat srcA, Mat srcB, Mat mask);
Mat MakeMask(int width, int height, int borderSize);
Mat Threshold(Mat src,int threshold);
void animateMask(Mat srcA, Mat srcB, int waitTimeMs);

int main() {

	char* source_windowA = "Source imageA";
	char* source_windowB = "Source imageB";
	char* Mixed_Window = "Mixed Image";
	char* gradient_Window = "Gradient Image";
	char* inverse_Window = "inverse Image";
	char* Add_Window = "Add Image";
	char* substract_Window = "substract Image";
	char* threshold_Window = "threshold Image";


	Mat F = imread("F.jpg", 0);
	Mat G = imread("G.jpg", 0);

	if (F.empty()) return -1;
	if (G.empty()) return -1;

	Mat addImages(F.rows, F.cols, CV_8U);
	Mat substractImages(F.rows, F.cols, CV_8U);
	Mat inverseImages(F.rows, F.cols, CV_8U);
	Mat blendImagesHorizontal(F.rows, F.cols, CV_8U);
	Mat blendImagesVertical(F.rows, F.cols, CV_8U);
	Mat maskMix(F.rows, F.cols, CV_8U);
	Mat mask(F.rows, F.cols, CV_8U);
	Mat threshold(F.rows, F.cols, CV_8U);

	addImages = blendMaterial(F, G, 0.5, addition);
	substractImages = blendMaterial(F, G, 0.5, substract);
	inverseImages = inverseMaterial(G);
	blendImagesHorizontal = blendMaterial(F, G, horizontal);
	blendImagesVertical = blendMaterial(F, G, vertical);
	mask = MakeMask(F.rows, G.cols,100);
	maskMix = Mask(F, G, mask);
	animateMask(F,G,100);
	threshold = Threshold(F,60);

	namedWindow(source_windowA, CV_WINDOW_AUTOSIZE);
	namedWindow(source_windowB, CV_WINDOW_AUTOSIZE);
	namedWindow(gradient_Window, CV_WINDOW_AUTOSIZE);
	namedWindow(inverse_Window, CV_WINDOW_AUTOSIZE);
	namedWindow(substract_Window, CV_WINDOW_AUTOSIZE);
	namedWindow(Add_Window, CV_WINDOW_AUTOSIZE);
	namedWindow(threshold_Window, CV_WINDOW_AUTOSIZE);

	imshow(source_windowA, F);
	imshow(source_windowB, G);
	imshow(Add_Window, addImages);
	imshow(substract_Window, substractImages);
	imshow(inverse_Window, inverseImages);
	imshow(gradient_Window, blendImagesVertical);
	imshow(threshold_Window, threshold);


	waitKey(0);
	return 0;
}

Mat blendMaterial(Mat srcA, Mat srcB, Direction dir) {
	if (srcA.cols != srcB.cols || srcA.rows != srcB.rows)
		return Mat();
	Mat dst(srcA.rows, srcA.cols, CV_8U);
	Mat gradient = GradientMap(dst, dir);
	for (int i = 0; i < srcA.rows; i++)
	{
		for (int j = 0; j < srcA.cols; j++)
		{
			float valueA = (float)(gradient.at<uchar>(i, j) / 255.0);
			float valueB = 1 - valueA;
			int aux = ((valueA * srcA.at<uchar>(i, j) + valueB * srcB.at<uchar>(i, j)) / 2);
			dst.at<uchar>(i, j) = aux;
		}
	}
	return dst;
}
Mat Threshold(Mat src,int threshold){
	if(threshold<0||threshold>255){
		cout<<"Error, threshold not in range"<<endl;
			return src;
	}
	Mat dst(image.rows, image.cols, CV_8U);
	for (int i = 0; i < dst.rows; i++)
	{
		for (int j = 0; j < dst.cols; j++)
		{
			dst.at<uchar>(i, j) = (uchar)(image.at<uchar>(i, j) > threshold) ? 255 : 0;
		}
	}
}
Mat inverseMaterial(Mat src) {
	Mat dst(src.rows, src.cols, CV_8U);
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			dst.at<uchar>(i, j) = 255 - src.at<uchar>(i, j);
		}
	}
	return dst;
}

Mat blendMaterial(Mat srcA, Mat srcB, double srcABelnd, Type operation)
{
	if (srcA.cols != srcB.cols || srcA.rows != srcB.rows) {
		return Mat();
	}

	Mat dst(srcA.rows, srcB.cols, CV_8U);

	if (srcABelnd < 0)srcABelnd = 0;
	if (srcABelnd > 1)srcABelnd = 1;

	float srcBBlend = 1 - srcABelnd;
	for (int i = 0; i < srcA.rows; i++)
	{
		for (int j = 0; j < srcA.cols; j++)
		{
			if (operation == addition) {
				int aux = ((srcABelnd*srcA.at<uchar>(i, j) + srcBBlend*srcB.at<uchar>(i, j)) / 2);
				dst.at<uchar>(i, j) = (aux > 255) ? 255 : aux;
			}
			else {
				int aux = ((srcABelnd*srcA.at<uchar>(i, j) - srcBBlend*srcB.at<uchar>(i, j)) / 2);
				dst.at<uchar>(i, j) = (aux < 0) ? 0 : aux;
			}

		}
	}
	return dst;
}

Mat GradientMap(Mat src, Direction dir)
{
	Mat gradient(src.rows, src.cols, CV_8U);
	switch (dir)
	{
	case horizontal:
		for (int i = 0; i < gradient.rows; i++)
		{
			for (int j = 0; j < gradient.cols; j++)
			{
				gradient.at<uchar>(i, j) = 255 - (j * 255 / gradient.cols);
			}
		}
		break;
	case vertical:
		for (int i = 0; i < gradient.rows; i++)
		{
			for (int j = 0; j < gradient.cols; j++)
			{
				gradient.at<uchar>(i, j) = 255 - (i * 255 / gradient.rows);
			}
		}
		break;
	}
	return gradient;
}

Mat Mask(Mat srcA, Mat srcB, Mat mask)
{
	if ((srcA.rows == mask.rows&&mask.rows == srcB.rows) && (srcA.cols == mask.cols&&mask.cols == srcB.cols)) {
		Mat dst(srcA.rows, srcB.cols, CV_8U);
		for (int i = 0; i < dst.rows; i++)
		{
			for (int j = 0; j < dst.cols; j++)
			{
				dst.at<uchar>(i, j) = (mask.at<uchar>(i, j) == 0) ? srcA.at<uchar>(i, j) : srcB.at<uchar>(i, j);
			}
		}
		return dst;
	}
	else
		return Mat();

}

Mat MakeMask(int width, int height, int borderSize) {
	Mat ej7(width, height, CV_8U);
	for (int i = 0; i < ej7.rows; i++) {
		for (int j = 0; j < ej7.cols; j++) {
			ej7.at<char>(i, j) = 0;
			if (i > borderSize && i < ej7.rows - borderSize &&j>borderSize&&j < ej7.cols - borderSize)
				ej7.at<char>(i, j) = 255;
			if (2 * borderSize < i && 2 * borderSize<j && ej7.rows - 2 * borderSize>i && ej7.cols - 2 * borderSize>j)
				ej7.at<char>(i, j) = 0;
		}
	}
	return ej7;
}

void animateMask(Mat srcA, Mat srcB, int waitTimeMs)
{
	char * animated_Mask = "Animated mask";
	namedWindow(animated_Mask, CV_WINDOW_AUTOSIZE);
	if ((srcA.rows == srcB.rows) && (srcA.cols == srcB.cols)) {
		for (int i = 10; i < 1080/2; i += 10) {
			Mat dst(srcA.rows, srcB.cols, CV_8U);
			dst = Mask(srcA, srcB, MakeMask(srcA.rows, srcA.cols, i));			
			imshow(animated_Mask, dst);
			waitKey(10);
		}
	}
}
