#pragma once
#include <iostream>
#include <string.h>
#include <math.h>
#include "opencv2\core\core.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\highgui\highgui.hpp"

using namespace cv;
using namespace std;

enum InterpolationMode { ClosestPixel, Lineal };
enum RotateMode { Roundup, Interpolate };
enum Direction { horizontal, vertical };
enum Type { addition, substract };

struct MouseParams
{
	Mat src;
	int x, y;
};
struct MultipliMatrixParameters {
	Mat src;
	float rotation,
		scaleX,
		scaleY,
		skewX,
		skewY;
	int translateX,
		translateY;
};
MultipliMatrixParameters mmp;
const float DEG2RAD = 0.0174533;
const float RAD2DEG = 57.2958;
const float PI = 3.14159265359;
const float E = 2.71828;
void Start(void *ptr);
Mat Translate(Mat src, int x, int y);
Mat Rotate(Mat src, float angle, RotateMode mode);
Mat Scale(Mat src, double cx, double cy, InterpolationMode mode);
Mat blendMaterial(Mat srcA, Mat srcB, Direction dir);
Mat inverseMaterial(Mat src);
Mat blendMaterial(Mat srcA, Mat srcB, double srcABelnd, Type operation);
Mat GradientMap(Mat src, Direction dir);
Mat Mask(Mat srcA, Mat srcB, Mat mask);
Mat MakeMask(int width, int height, int borderSize);
Mat Threshold(Mat src, int threshold);
Mat EqualizeMaterial(Mat src);
int interpolate1D(float u, float v, Mat src);
Mat Skew(Mat src, float strength, Direction dir);
Mat MedianFilter(Mat src);
Mat MeanFilter(Mat src);
int AverageValue(int window[]);
void insertionSort(int window[]);
void CallBackFunc(int event, int xl, int yl, int flags, void* userdata);
Mat Wave(Mat src, Direction dir);
int sign(float num);
Mat warp(Mat src, Direction dir, int x, int y);
Mat whirl(Mat src, int x, int y);
void transformationMatrix();
Mat Gauss(Mat src, int size, float sigma);
Mat CreateGaussMask(int size, float sigma);
void TrackRota(int v, void*ptr);
void TrackShearX(int v, void*ptr);
void TrackShearY(int v, void*ptr);
void TrackScaleX(int v, void*ptr);
void TrackScaleY(int v, void*ptr);
void TrackTransX(int v, void*ptr);
void TrackTransY(int v, void*ptr);

void Start(void *ptr) {
	MultipliMatrixParameters *MulMaPar = (MultipliMatrixParameters*)ptr;
	mmp.src = MulMaPar->src;
	mmp.scaleX = 1;
	mmp.scaleY = 1;
}
Mat Translate(Mat src, int x, int y) {
	Mat dst(src.rows, src.cols, CV_8U, Scalar(0));

	for (int i = 0; i < dst.rows; i++)
	{
		for (int j = 0; j < dst.cols; j++) {
			int newX = i + y;
			int newY = j + x;
			if ((src.rows > newX && newX >= 0) && (src.cols > newY && newY >= 0))
				dst.at<uchar>(i, j) = src.at<uchar>(newX, newY);
		}
	}
	return dst;
}
Mat Rotate(Mat src, float angle, RotateMode mode) {
	Mat dst(src.rows, src.cols, CV_8U, Scalar(0));

	for (int i = 0; i < dst.rows; i++)
	{
		for (int j = 0; j < dst.cols; j++) {
			float x = j - src.rows / 2;
			float y = src.cols / 2 - i;
			float u = x * cos(angle*DEG2RAD) + y * sin(angle*DEG2RAD);
			float v = -x * sin(angle*DEG2RAD) + y * cos(angle*DEG2RAD);
			float i0 = u + src.rows / 2;
			float j0 = -v + src.cols / 2;
			switch (mode)
			{
			case Roundup:
				floor(i0);
				floor(j0);
				if (i0 >= 0 && i0 < dst.rows&&j0 >= 0 && j0 < dst.cols)
					dst.at<uchar>(i, j) = src.at<uchar>(i0, j0);
				else
					dst.at<uchar>(i, j) = 0;
				break;
			case Interpolate:
				dst.at<uchar>(i, j) = interpolate1D(i0, j0, src);
				break;
			}

		}
	}
	return dst;
}
Mat Scale(Mat src, double cx, double cy, InterpolationMode mode) {
	if (cx <= 0 || cy <= 0) {
		cout << "Cx AND cy Must be greater than 0" << endl;
		return src;
	}

	Mat dst(src.rows*cx, src.cols*cy, CV_8U);
	for (int i = 0; i < dst.rows; i++)
	{
		for (int j = 0; j < dst.cols; j++)
		{
			switch (mode)
			{
			case ClosestPixel:
				dst.at<uchar>(i, j) = src.at<uchar>(i / cx, j / cy);

				break;
			case Lineal:
				float v = (float)i / cx;
				float u = (float)j / cy;
				dst.at<uchar>(i, j) = interpolate1D(u, v, src);
				break;
			}
		}
	}
	return dst;
}
int interpolate1D(float u, float v, Mat src) {
	float a00 = floor(u);
	float a01 = ceil(u);
	float b0 = floor(v);
	float b1 = ceil(v);

	if (b0 >= 0 && b0 < src.rows&&b1 >= 0 && b1 < src.rows) {
		if (a00 >= 0 && a00 < src.cols &&a01 >= 0 && a01 < src.cols) {


			float a, fa, fa0, fa1;

			a = u - a00;
			fa0 = (1 - a)*src.at<uchar>(b0, a00) + a*src.at<uchar>(b0, a01);
			fa1 = (1 - a)*src.at<uchar>(b1, a00) + a*src.at<uchar>(b1, a01);
			a = v - b0;
			fa = (1 - a)*fa0 + a*fa1;
			return  (uchar)fa;
		}
		else return 0;
	}
	else return 0;
}
Mat blendMaterial(Mat srcA, Mat srcB, Direction dir) {
	if (srcA.cols != srcB.cols || srcA.rows != srcB.rows)
		return Mat();
	Mat dst(srcA.rows, srcA.cols, CV_8U);
	Mat gradient = GradientMap(dst, dir);
	for (int i = 0; i < srcA.rows; i++)
	{
		for (int j = 0; j < srcA.cols; j++)
		{
			float valueA = (float)(gradient.at<uchar>(i, j) / 255.0);
			float valueB = 1 - valueA;
			int aux = ((valueA * srcA.at<uchar>(i, j) + valueB * srcB.at<uchar>(i, j)) / 2);
			dst.at<uchar>(i, j) = aux;
		}
	}
	return dst;
}
Mat Threshold(Mat src, int threshold) {
	if (threshold < 0 || threshold>255) {
		cout << "Error, threshold not in range" << endl;
		return src;
	}
	Mat dst(src.rows, src.cols, CV_8U);
	for (int i = 0; i < dst.rows; i++)
	{
		for (int j = 0; j < dst.cols; j++)
		{
			dst.at<uchar>(i, j) = (uchar)(src.at<uchar>(i, j) > threshold) ? 255 : 0;
		}
	}
}
Mat inverseMaterial(Mat src) {
	Mat dst(src.rows, src.cols, CV_8U);
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			dst.at<uchar>(i, j) = 255 - src.at<uchar>(i, j);
		}
	}
	return dst;
}

Mat blendMaterial(Mat srcA, Mat srcB, double srcABelnd, Type operation)
{
	if (srcA.cols != srcB.cols || srcA.rows != srcB.rows) {
		return Mat();
	}

	Mat dst(srcA.rows, srcB.cols, CV_8U);

	if (srcABelnd < 0)srcABelnd = 0;
	if (srcABelnd > 1)srcABelnd = 1;

	float srcBBlend = 1 - srcABelnd;
	for (int i = 0; i < srcA.rows; i++)
	{
		for (int j = 0; j < srcA.cols; j++)
		{
			if (operation == addition) {
				int aux = ((srcABelnd*srcA.at<uchar>(i, j) + srcBBlend*srcB.at<uchar>(i, j)) / 2);
				dst.at<uchar>(i, j) = (aux > 255) ? 255 : aux;
			}
			else {
				int aux = ((srcABelnd*srcA.at<uchar>(i, j) - srcBBlend*srcB.at<uchar>(i, j)) / 2);
				dst.at<uchar>(i, j) = (aux < 0) ? 0 : aux;
			}

		}
	}
	return dst;
}

Mat GradientMap(Mat src, Direction dir)
{
	Mat gradient(src.rows, src.cols, CV_8U);
	switch (dir)
	{
	case horizontal:
		for (int i = 0; i < gradient.rows; i++)
		{
			for (int j = 0; j < gradient.cols; j++)
			{
				gradient.at<uchar>(i, j) = 255 - (j * 255 / gradient.cols);
			}
		}
		break;
	case vertical:
		for (int i = 0; i < gradient.rows; i++)
		{
			for (int j = 0; j < gradient.cols; j++)
			{
				gradient.at<uchar>(i, j) = 255 - (i * 255 / gradient.rows);
			}
		}
		break;
	}
	return gradient;
}

Mat Mask(Mat srcA, Mat srcB, Mat mask)
{
	if ((srcA.rows == mask.rows&&mask.rows == srcB.rows) && (srcA.cols == mask.cols&&mask.cols == srcB.cols)) {
		Mat dst(srcA.rows, srcB.cols, CV_8U);
		for (int i = 0; i < dst.rows; i++)
		{
			for (int j = 0; j < dst.cols; j++)
			{
				dst.at<uchar>(i, j) = (mask.at<uchar>(i, j) == 0) ? srcA.at<uchar>(i, j) : srcB.at<uchar>(i, j);
			}
		}
		return dst;
	}
	else
		return Mat();

}

Mat MakeMask(int width, int height, int borderSize) {
	Mat ej7(width, height, CV_8U);
	for (int i = 0; i < ej7.rows; i++) {
		for (int j = 0; j < ej7.cols; j++) {
			ej7.at<char>(i, j) = 0;
			if (i > borderSize && i < ej7.rows - borderSize &&j>borderSize&&j < ej7.cols - borderSize)
				ej7.at<char>(i, j) = 255;
			if (2 * borderSize < i && 2 * borderSize<j && ej7.rows - 2 * borderSize>i && ej7.cols - 2 * borderSize>j)
				ej7.at<char>(i, j) = 0;
		}
	}
	return ej7;
}

void animateMask(Mat srcA, Mat srcB, int waitTimeMs)
{
	char * animated_Mask = "Animated mask";
	namedWindow(animated_Mask, CV_WINDOW_AUTOSIZE);
	if ((srcA.rows == srcB.rows) && (srcA.cols == srcB.cols)) {
		for (int i = 10; i < 1080 / 2; i += 10) {
			Mat dst(srcA.rows, srcB.cols, CV_8U);
			dst = Mask(srcA, srcB, MakeMask(srcA.rows, srcA.cols, i));
			imshow(animated_Mask, dst);
			waitKey(10);
		}
	}
}
Mat EqualizeMaterial(Mat src) {
	Mat dst(src.rows, src.cols, CV_8U);

	int hist[256] = {};
	int cdf[256] = {};

	for (int i = 0; i < src.rows; i++)
		for (int j = 0; j < src.cols; j++)
			hist[src.at<uchar>(i, j)]++;

	cdf[0] = hist[0];
	for (int i = 1; i < 256; i++)
		cdf[i] = cdf[i - 1] + hist[i];

	for (int i = 1; i < 256; i++)
		hist[i] = round((((float)cdf[i]) / (src.rows*src.cols)) * 254) + 1;
	hist[0] = 0;

	for (int i = 0; i < src.rows; i++) {
		for (int j = 0; j < src.cols; j++) {
			dst.at<uchar>(i, j) = hist[src.at<uchar>(i, j)];
		}
	}
	return dst;
}
Mat Skew(Mat src, float strength, Direction dir) {
	Mat dst(src.rows, src.cols, CV_8U, Scalar(0));
	float u, v;
	for (int i = 0; i < dst.rows; i++)
	{
		for (int j = 0; j < dst.cols; j++) {
			float x = j - src.rows / 2;
			float y = dst.cols / 2 - i;
			switch (dir)
			{
			case horizontal:
				u = x - strength*y;
				v = y;
				break;
			case vertical:
				u = x;
				v = y - strength*x;
				break;
			}
			float i0 = u + src.rows / 2;
			float j0 = -v + src.cols / 2;
			dst.at<uchar>(i, j) = interpolate1D((int)i0, (int)j0, src);
		}
	}
	return dst;
}
Mat MedianFilter(Mat src) {
	Mat dst(src.rows, src.cols, CV_8U, Scalar(0));
	int window[9];

	for (int i = 1; i < src.rows - 1; i++) {
		for (int j = 1; j < src.cols - 1; j++) {

			window[0] = src.at<uchar>(i - 1, j - 1);
			window[1] = src.at<uchar>(i, j - 1);
			window[2] = src.at<uchar>(i + 1, j - 1);
			window[3] = src.at<uchar>(i - 1, j);
			window[4] = src.at<uchar>(i, j);
			window[5] = src.at<uchar>(i + 1, j);
			window[6] = src.at<uchar>(i - 1, j + 1);
			window[7] = src.at<uchar>(i, j + 1);
			window[8] = src.at<uchar>(i + 1, j + 1);

			insertionSort(window);

			dst.at<uchar>(i, j) = window[4];
		}
	}
	return dst;
}
Mat MeanFilter(Mat src) {
	Mat dst(src.rows, src.cols, CV_8U, Scalar(0));
	int window[9];

	for (int i = 1; i < src.rows - 1; i++) {
		for (int j = 1; j < src.cols - 1; j++) {

			window[0] = src.at<uchar>(i - 1, j - 1);
			window[1] = src.at<uchar>(i, j - 1);
			window[2] = src.at<uchar>(i + 1, j - 1);
			window[3] = src.at<uchar>(i - 1, j);
			window[4] = src.at<uchar>(i, j);
			window[5] = src.at<uchar>(i + 1, j);
			window[6] = src.at<uchar>(i - 1, j + 1);
			window[7] = src.at<uchar>(i, j + 1);
			window[8] = src.at<uchar>(i + 1, j + 1);


			dst.at<uchar>(i, j) = AverageValue(window);
		}
	}
	return dst;
}
int AverageValue(int window[]) {
	int avg = 0;
	for (int i = 0; i < 9; i++)
	{
		avg += window[i];
	}
	avg /= 9;
	return avg;
}
void insertionSort(int window[]) {
	int temp, i, j;
	for (i = 0; i < 9; i++) {
		temp = window[i];
		for (j = i - 1; j >= 0 && temp < window[j]; j--) {
			window[j + 1] = window[j];
		}
		window[j + 1] = temp;
	}
}
Mat Wave(Mat src, Direction dir) {
	Mat dst(src.rows, src.cols, CV_8U, Scalar(0));
	float u, v;
	for (int i = 0; i < dst.rows; i++)
	{
		for (int j = 0; j < dst.cols; j++) {
			float x = j - src.rows / 2;
			float y = dst.cols / 2 - i;
			switch (dir)
			{
			case horizontal:
				u = x;
				v = y + 80 * sin((2 * PI*x) / 128);
				break;
			case vertical:
				u = x + 20 * sin((2 * PI*y) / 128);
				v = y;
				break;
			}

			float i0 = u + src.rows / 2;
			float j0 = -v + src.cols / 2;
			dst.at<uchar>(i, j) = interpolate1D((int)i0, (int)j0, src);
		}
	}
	return dst;
}
Mat warp(Mat src, Direction dir, int x, int y) {
	Mat dst(src.rows, src.cols, CV_8U, Scalar(0));
	float i0, j0;
	for (int i = 0; i < dst.rows; i++)
	{
		for (int j = 0; j < dst.cols; j++) {
			switch (dir)
			{
			case horizontal:
				i0 = sign(i - x) * (pow((i - x), 2) / x) + x;
				dst.at<uchar>(i, j) = interpolate1D(j, i0, src);
				break;
			case vertical:
				j0 = sign(j - y) * (pow((j - y), 2) / y) + y;
				dst.at<uchar>(i, j) = interpolate1D(j0, i, src);
				break;
			default:
				break;
			}

		}
	}
	imshow("Source imageA", dst);
	return dst;
}
Mat whirl(Mat src, int x, int y) {
	Mat dst(src.rows, src.cols, CV_8U, Scalar(0));
	for (int i = 0; i < dst.rows; i++)
	{
		for (int j = 0; j < dst.cols; j++) {

			float r = sqrt(pow((i - x), 2) + pow((j - y), 2));
			float angle = ((PI*r) / 5)*DEG2RAD;
			float i0 = ((i - x)*(cos(angle))) + ((j - y)* (sin(angle))) + x;
			float j0 = -((i - x) * (sin(angle))) + ((j - y)*(cos(angle))) + y;

			dst.at<uchar>(i, j) = interpolate1D(j0, i0, src);
		}
	}
	imshow("Source imageA", dst);
	return dst;
}
int sign(float num) {
	return (num > 0) ? 1 : (num == 0) ? 0 : -1;
}
void transformationMatrix()
{

	Mat dst(mmp.src.rows, mmp.src.cols, CV_8U, Scalar(0));
	//RotationMatrix
	Mat rotMatrix(3, 3, CV_32F);
	rotMatrix.at<float>(0, 0) = cos(mmp.rotation);
	rotMatrix.at<float>(0, 1) = sin(mmp.rotation);
	rotMatrix.at<float>(0, 2) = 0;
	rotMatrix.at<float>(1, 0) = -sin(mmp.rotation);
	rotMatrix.at<float>(1, 1) = cos(mmp.rotation);
	rotMatrix.at<float>(1, 2) = 0;
	rotMatrix.at<float>(2, 0) = 0;
	rotMatrix.at<float>(2, 1) = 0;
	rotMatrix.at<float>(2, 2) = 1;

	//ShearMatrix
	Mat skewMatrix(3, 3, CV_32F);

	skewMatrix.at<float>(0, 0) = 1;
	skewMatrix.at<float>(0, 1) = mmp.skewX;
	skewMatrix.at<float>(0, 2) = 0;
	skewMatrix.at<float>(1, 0) = mmp.skewY;
	skewMatrix.at<float>(1, 1) = 1;
	skewMatrix.at<float>(1, 2) = 0;
	skewMatrix.at<float>(2, 0) = 0;
	skewMatrix.at<float>(2, 1) = 0;
	skewMatrix.at<float>(2, 2) = 1;

	//ScaleMatrix
	Mat scaleMatrix(3, 3, CV_32F);

	scaleMatrix.at<float>(0, 0) = mmp.scaleX;
	scaleMatrix.at<float>(0, 1) = 0;
	scaleMatrix.at<float>(0, 2) = 0;
	scaleMatrix.at<float>(1, 0) = 0;
	scaleMatrix.at<float>(1, 1) = mmp.scaleY;
	scaleMatrix.at<float>(1, 2) = 0;
	scaleMatrix.at<float>(2, 0) = 0;
	scaleMatrix.at<float>(2, 1) = 0;
	scaleMatrix.at<float>(2, 2) = 1;

	//traslationMatrix
	Mat ranslateMatrix = (Mat_<float>(3, 3) << 1, 0, mmp.translateX, 0, 1, mmp.translateY, 0, 0, 1);
	Mat finalMatrix = rotMatrix;
	finalMatrix *= skewMatrix;
	finalMatrix *= scaleMatrix;
	finalMatrix *= ranslateMatrix;

	for (int i = 0; i < dst.rows; i++)
	{
		for (int j = 0; j < dst.cols; j++) {
			float x = j - mmp.src.rows / 2;
			float y = mmp.src.cols / 2 - i;
			Mat MyVector = (Mat_<float>(3, 1) << x, y, 1);
			Mat temp = finalMatrix*MyVector;
			//cout << temp.at<float>(0, 0) << " | " << temp.at<float>(1, 0) << endl;
			//dst.at<uchar>(i, j) = mmp.src.at<uchar>((int)temp.at<float>(0, 0), (int)temp.at<float>(1, 0));

			dst.at<uchar>(i, j) = interpolate1D(temp.at<float>(0, 0) + mmp.src.rows / 2, -temp.at<float>(1, 0) + mmp.src.cols / 2, mmp.src);
		}
	}
	imshow("my window", dst);
}
Mat Gauss(Mat src, int size,float sigma)
{
	float res;
	
	Mat dst(src.rows, src.cols, CV_8U, Scalar(0));
	/*Mat mask = (Mat_<int>(5, 5) << 1, 4, 7, 4, 1,
		4, 16, 26, 16, 4,
		7, 26, 41, 26, 7,
		4, 16, 26, 16, 4,
		1, 4, 7, 4, 1);
	int size = 5;*/
	Mat mask = CreateGaussMask(size,sigma);
	size = (size - 1) / 2;
	for (int j = size; j < src.rows - size; j++)
		for (int i = size; i < src.cols - size; i++) {
			float suma = 0;
			float dividend = 0;
			for (int l = -size; l <= size; l++)
				for (int k = -size; k <= size; k++) {
					dividend += mask.at<float>(size + l, size + k);
					suma = suma + src.at<uchar>(j + l, i + k)* mask.at<float>(size + l, size + k);
				}
			res = suma/dividend;

			dst.at<uchar>(j, i) = res;

		}
	return dst;
}
Mat CreateGaussMask(int size,float sigma) {
	Mat dst(size, size, CV_32F, Scalar(0));
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			int x = i-size / 2;
			int y = size / 2-j;
			float a = (1 / (sigma*sigma * 2 * PI));
			float b = pow(E, -(((float)x*(float)x + (float)y * (float)y) / (2 * sigma*sigma)));
			dst.at<float>(i, j) = a*b;
		}
	}
	return dst;
}
void Applybutton(int state, void*ptr) {
	transformationMatrix();
}
void TrackRota(int v, void*ptr) {
	mmp.rotation = v*DEG2RAD;
	//transformationMatrix();

}
void TrackShearX(int v, void*ptr) {
	mmp.skewX = v;
	//transformationMatrix();
}
void TrackShearY(int v, void*ptr) {
	mmp.skewY = v;
	//transformationMatrix();
}
void TrackScaleX(int v, void*ptr) {
	mmp.scaleX = pow(v, -1);
	//transformationMatrix();
}
void TrackScaleY(int v, void*ptr) {
	mmp.scaleY = pow(v, -1);
	//transformationMatrix();
}
void TrackTransX(int v, void*ptr) {
	MultipliMatrixParameters* multMatrPar = (MultipliMatrixParameters*)ptr;
	mmp.translateX = -v + mmp.src.cols / 2;
	//transformationMatrix();
}
void TrackTransY(int v, void*ptr) {
	MultipliMatrixParameters* multMatrPar = (MultipliMatrixParameters*)ptr;
	mmp.translateY = -v + mmp.src.rows / 2;
	//transformationMatrix();
}
void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
	if (event == EVENT_LBUTTONDOWN)
	{
		cout << "Left button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
		// Mount back the parameters
		MouseParams* mp = (MouseParams*)userdata;
		Mat & src = mp->src;
		warp(src, horizontal, y, x);
	}
	else if (event == EVENT_RBUTTONDOWN)
	{
		cout << "Right button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
		MouseParams* mp = (MouseParams*)userdata;
		Mat & src = mp->src;
		warp(src, vertical, y, x);
	}
	else if (event == EVENT_MBUTTONDOWN)
	{
		cout << "Middle button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
		MouseParams* mp = (MouseParams*)userdata;
		Mat & src = mp->src;
		whirl(src, y, x);
	}
}