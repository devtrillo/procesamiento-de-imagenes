#include "Functions.h"

int main() {


	char* source_windowA = "Source imageA";
	char* binWindow = "BinaryWindow";
	char* eroWindow = "Erosion";
	Mat F = imread("F.jpg", 0);
	Mat FThres = Mat();
	if (F.empty()) return -1;


	threshold(F, FThres, 50, 255, 0);
	namedWindow(binWindow, 0);
	imshow(binWindow, FThres);


	namedWindow(source_windowA, 0);
	imshow(source_windowA, F);

	int erosionSize = 2;
	Mat element = getStructuringElement(MORPH_ELLIPSE, Size(2 * erosionSize + 1, 2 * erosionSize + 1), Point(erosionSize, erosionSize));

	Mat result = F.clone();
	erode(FThres, result, element);

	namedWindow(eroWindow, 0);
	imshow(eroWindow, result);
	F = Gauss(F, 21, 4);
	waitKey(0);
	return 0;
}