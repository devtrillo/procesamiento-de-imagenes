#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "cameraparams.h"
#include "patterndetector.h"

using namespace std;
using namespace cv;
using namespace ARma;

#define PAT_SIZE 64//equal to pattern_size variable (see below)
#define SAVE_VIDEO 0 //if true, it saves the video in "output.avi"
#define NUM_OF_PATTERNS 3// define the number of patterns you want to use

char* filename1 = "pattern1.jpg";//id=1
char* filename2 = "pattern2.jpg";//id=2
char* filename3 = "pattern3.jpg";//id=3

static int loadPattern(const char*, std::vector<cv::Mat>&, int&);

int main() {

	std::vector<cv::Mat> patternLibrary;
	std::vector<Pattern> detectedPattern;
	int patternCount = 0;

	/*create patterns' library using rotated versions of patterns
	*/
	loadPattern(filename1, patternLibrary, patternCount);
#if (NUM_OF_PATTERNS==2)
	loadPattern(filename2, patternLibrary, patternCount);
#endif
#if (NUM_OF_PATTERNS==3)
	loadPattern(filename2, patternLibrary, patternCount);
	loadPattern(filename3, patternLibrary, patternCount);
#endif


	cout << patternCount << " patterns are loaded." << endl;


	int norm_pattern_size = PAT_SIZE;
	double fixed_thresh = 40;
	double adapt_thresh = 5;//non-used with FIXED_THRESHOLD mode
	int adapt_block_size = 45;//non-used with FIXED_THRESHOLD mode
	double confidenceThreshold = 0.35;
	int mode = 2;//1:FIXED_THRESHOLD, 2: ADAPTIVE_THRESHOLD

	PatternDetector myDetector(fixed_thresh, adapt_thresh, adapt_block_size, confidenceThreshold, norm_pattern_size, mode);

	//CvCapture* capture = cvCaptureFromCAM(0);
	VideoCapture capture(1);
	if (!capture.isOpened())return -1;

#if (SAVE_VIDEO)
	CvVideoWriter *video_writer = cvCreateVideoWriter("output.avi", -1, 25, cvSize(640, 480));
#endif

	Mat imgMat;
	Mat imgMat_bin;
	int k = 0;
	namedWindow("result", 0);
	while (k != 'q') {

		//Mat imgMat;
		capture >> imgMat;
		cvtColor(imgMat, imgMat_bin, CV_BGR2GRAY);
		//threshold(imgMat_bin, imgMat_bin, 45, 255,0);
		double tic = (double)cvGetTickCount();


		//run the detector
		myDetector.detect(imgMat_bin, cameraMatrix, distortions, patternLibrary, detectedPattern);

		double toc = (double)cvGetTickCount();
		double detectionTime = (toc - tic) / ((double)cvGetTickFrequency() * 1000);
		//cout << "Detected Patterns: " << detectedPattern.size() << endl;
		//cout << "Detection time: " << detectionTime << endl;

		//augment the input frame (and print out the properties of pattern if you want)
		//int IdsWatched[3] = { 0 };
		for (unsigned int i = 0; i < detectedPattern.size(); i++) {
			//if (IdsWatched[detectedPattern.at(i).id] == 0) {
				//IdsWatched[detectedPattern.at(i).id]++;
				//detectedPattern.at(i).showPattern();
			detectedPattern.at(i).draw(imgMat, cameraMatrix, distortions);
			//}
		}
		/*for (int i = 0; i < 3; i++)
			IdsWatched[i] = 0;*/

#if (SAVE_VIDEO)
		cvWriteFrame(video_writer, &((IplImage)imgMat));
#endif
		imshow("result", imgMat);
		k = waitKey(20);
		detectedPattern.clear();
	}

#if (SAVE_VIDEO)
	cvReleaseVideoWriter(&video_writer);
#endif
	//cvReleaseCapture(&capture);
	capture.release();
	return 0;

}

int loadPattern(const char* filename, std::vector<cv::Mat>& library, int& patternCount) {
	Mat img = imread(filename, 0);
	if (!img.data) {
		printf("%s File not found", filename);
		return -1;
	}
	if (img.cols != img.rows) {
		printf("Not a square pattern");
		return -1;
	}

	int msize = PAT_SIZE;

	Mat src(msize, msize, CV_8UC1);
	Point2f center((msize - 1) / 2.0f, (msize - 1) / 2.0f);
	Mat rot_mat(2, 3, CV_32F);

	resize(img, src, Size(msize, msize));
	Mat subImg = src(Range(msize / 4, 3 * msize / 4), Range(msize / 4, 3 * msize / 4));
	library.push_back(subImg);

	rot_mat = getRotationMatrix2D(center, 90, 1.0);

	for (int i = 1; i < 4; i++) {
		Mat dst = Mat(msize, msize, CV_8UC1);
		rot_mat = getRotationMatrix2D(center, -i * 90, 1.0);
		warpAffine(src, dst, rot_mat, Size(msize, msize));
		Mat subImg = dst(Range(msize / 4, 3 * msize / 4), Range(msize / 4, 3 * msize / 4));
		library.push_back(subImg);
	}

	patternCount++;
	return 1;
}

