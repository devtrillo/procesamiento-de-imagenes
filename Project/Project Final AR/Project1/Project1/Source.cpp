#include "Functions.h"

/*int main() {
	char* source_windowA = "Source imageA";
	char* destWindow = "Gauss_Mask21_Sigma1.jpg";
	Mat F = imread("F.jpg", 0);

	if (F.empty()) return -1;

	namedWindow(source_windowA, 0);


	imshow(source_windowA, F);

	imwrite(destWindow, Gauss(F, 21, 1));
	destWindow = "Gauss_Mask21_Sigma2.jpg";
	imwrite(destWindow, Gauss(F, 21, 2));
	destWindow = "Gauss_Mask21_Sigma10.jpg";
	imwrite(destWindow, Gauss(F, 21, 10));
	destWindow = "Gauss_Mask21_Sigma19.jpg";
	imwrite(destWindow, Gauss(F, 21, 19));
	destWindow = "Gauss_Mask11_Sigma1.jpg";
	imwrite(destWindow, Gauss(F, 11, 1));
	destWindow = "Gauss_Mask11_Sigma2.jpg";
	imwrite(destWindow, Gauss(F, 11, 2));
	destWindow = "Gauss_Mask11_Sigma3.jpg";
	imwrite(destWindow, Gauss(F, 11, 3));
	destWindow = "Gauss_Mask11_Sigma4.jpg";
	imwrite(destWindow, Gauss(F, 11, 4));
	destWindow = "Gauss_Mask5_Sigma10.jpg";
	imwrite(destWindow, Gauss(F, 5, 10));
	destWindow = "Gauss_Mask5_Sigma25.jpg";
	imwrite(destWindow, Gauss(F, 5, 25));
	destWindow = "Gauss_Mask5_Sigma1.jpg";
	imwrite(destWindow, Gauss(F, 5, 1));
	destWindow = "Gauss_Mask5_Sigma5.jpg";
	imwrite(destWindow, Gauss(F, 5, 5));
	destWindow = "Gauss_Mask31_Sigma1.jpg";
	imwrite(destWi+ndow, Gauss(F, 31, 1));
	destWindow = "Gauss_Mask31_Sigma2.jpg";
	imwrite(destWindow, Gauss(F, 31, 2));
	destWindow = "Gauss_Mask31_Sigma3.jpg";
	imwrite(destWindow, Gauss(F, 31, 3));
	destWindow = "Gauss_Mask31_Sigma4.jpg";
	imwrite(destWindow, Gauss(F, 31, 4));

	namedWindow(destWindow, 0);


	imshow(destWindow, F);



	char* source_windowA = "Source imageA";
	char* binWindow = "BinaryWindow";
	char* eroWindow = "Erosion";
	Mat F = imread("F.jpg", 0);
	Mat FThres = Mat();
	if (F.empty()) return -1;


	threshold(F, FThres, 50, 255, 0);
	namedWindow(binWindow, 0);
	imshow(binWindow, FThres);


	namedWindow(source_windowA, 0);
	imshow(source_windowA, F);

	int erosionSize = 2;
	Mat element = getStructuringElement(MORPH_ELLIPSE, Size(2 * erosionSize + 1, 2 * erosionSize + 1), Point(erosionSize, erosionSize));

	Mat result = F.clone();
	erode(FThres, result, element);

	namedWindow(eroWindow, 0);
	imshow(eroWindow, result);
	F = Gauss(F, 21, 4);
	waitKey(0);
	return 0;
}*/

//Codigo de borde
/*int main()
{
	Mat image = imread("Mona.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	if (image.empty())
		return -1;
	namedWindow("imagen", CV_WINDOW_NORMAL);
	imshow("imagen", image);

	Mat filter1 = image.clone();


	GaussianBlur(image, filter1, Size(11, 11), 2, 2, BORDER_DEFAULT);
	namedWindow("Gausiano", CV_WINDOW_NORMAL);
	imshow("Gausiano", filter1);
	waitKey(0);

	Mat border1 = image.clone();

	//for (int j = 0; j < image.rows; j++)
	//for (int i = 0; i < image.cols; i++)
	//{
	//	int val1 = image.at<uchar>(j, i);
	//	int val2 = filter1.at<uchar>(j, i);
	//	int res = abs(val1 - val2);
	//
	//	border1.at<uchar>(j, i) = (uchar)res;

	//}

	border1 = image - filter1;
	namedWindow("Differ", CV_WINDOW_NORMAL);
	imshow("Differ", border1);
	waitKey(0);

	Mat resal = image.clone();
	resal = image + border1;//*0.5;
	namedWindow("Resaltada", CV_WINDOW_NORMAL);
	imshow("Resaltada", resal);
	waitKey(0);
}*/

//codigo de gradiente
/*int main()
{
	Mat image = imread("Mona.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	if (image.empty())
		return -1;
	namedWindow("imagen", CV_WINDOW_NORMAL);
	imshow("imagen", image);

	Mat filter1 = image.clone();


	GaussianBlur(image, filter1, Size(3, 3), 0, 0, BORDER_DEFAULT);
	namedWindow("Gausiano", CV_WINDOW_NORMAL);
	imshow("Gausiano", filter1);
	waitKey(0);

	Mat border1 = image.clone();
	filter1.copyTo(image);

	for (int j = 0; j < image.rows - 1; j++)
		for (int i = 0; i < image.cols - 1; i++)
		{
			int val1 = image.at<uchar>(j, i);
			int val2 = image.at<uchar>(j, i + 1);
			int res = abs(val2 - val1);
			border1.at<uchar>(j, i) = (uchar)res;

		}


	Mat border2 = image.clone();

	for (int j = 0; j < image.rows - 1; j++)
		for (int i = 0; i < image.cols - 1; i++)
		{
			int val1 = image.at<uchar>(j, i);
			int val2 = image.at<uchar>(j + 1, i);
			int res = abs(val2 - val1);
			border2.at<uchar>(j, i) = (uchar)res;

		}

	namedWindow("gradx", CV_WINDOW_NORMAL);
	imshow("gradx", border1);
	waitKey(0);


	namedWindow("grady", CV_WINDOW_NORMAL);
	imshow("grady", border2);
	waitKey(0);

	threshold(border1, border1, 10, 255, 0);
	namedWindow("gradx", CV_WINDOW_NORMAL);
	imshow("gradx", border1);
	waitKey(0);

	threshold(border2, border2, 10, 255, 0);
	namedWindow("grady", CV_WINDOW_NORMAL);
	imshow("grady", border2);
	waitKey(0);
}*/
//Canny
int main()
{
	Mat image = imread("dessertS.jpeg", CV_LOAD_IMAGE_GRAYSCALE);
	if (image.empty())
		return -1;
	namedWindow("imagen", CV_WINDOW_NORMAL);
	imshow("imagen", image);

	Mat filter1 = image.clone();


	GaussianBlur(image, filter1, Size(11, 11), 3, 1, BORDER_DEFAULT);
	namedWindow("Gausiano", CV_WINDOW_NORMAL);
	imshow("Gausiano", filter1);
	waitKey(0);

	Mat border1 = image.clone();

	filter1.copyTo(image);

	Sobel(filter1, border1, CV_8U, 2, 0, 3, 1, 0, BORDER_DEFAULT);
	convertScaleAbs(border1, border1);
	namedWindow("gradx", CV_WINDOW_NORMAL);
	imshow("gradx", border1);
	waitKey(0);

	Mat border2 = image.clone();
	Sobel(filter1, border2, CV_8U, 0, 2, 3, 1, 0, BORDER_DEFAULT);
	convertScaleAbs(border2, border2);
	namedWindow("grady", CV_WINDOW_NORMAL);
	imshow("grady", border2);
	waitKey(0);


	Mat border3(image.rows, image.cols, CV_16S);
	addWeighted(border1, 0.5, border2, 0.5, 0, border3);

	//Sobel(filter1, border3, CV_16S, 2, 2, 3, 1, 0, BORDER_DEFAULT);
	convertScaleAbs(border3, border3);
	//threshold(border3, border3, 15, 255, 0);

	namedWindow("sobel", CV_WINDOW_NORMAL);
	imshow("sobel", border3);
	waitKey(0);

	Mat border4(image.rows, image.cols, CV_16S);
	Laplacian(filter1, border4, CV_16S, 3, 1, 0, 4);
	convertScaleAbs(border4, border4);
	namedWindow("Laplace", CV_WINDOW_NORMAL);
	imshow("Laplace", border4);
	waitKey(0);

	Mat border5(image.rows, image.cols, CV_16S);
	Canny(filter1, border5, 10, 70, 3);
	convertScaleAbs(border5, border5);
	namedWindow("Canny", CV_WINDOW_NORMAL);
	imshow("Canny", border5);
	waitKey(0);

}