#include "pattern.h"
#include <iostream>
using namespace cv;
using namespace std;

namespace ARma {

	Pattern::Pattern(double param1) {
		id = -1;
		size = param1;
		orientation = -1;
		confidence = -1;

		rotVec = (Mat_<float>(3, 1) << 0, 0, 0);
		transVec = (Mat_<float>(3, 1) << 0, 0, 0);
		rotMat = Mat::eye(3, 3, CV_32F);
	}

	//convert rotation vector to rotation matrix (if you want to proceed with other libraries)
	void Pattern::rotationMatrix(const Mat& rotation_vector, Mat& rotation_matrix)
	{
		Rodrigues(rotation_vector, rotation_matrix);
	}

	void Pattern::showPattern()
	{
		cout << "Pattern ID: " << id << endl;
		cout << "Pattern Size: " << size << endl;
		cout << "Pattern Confedince Value: " << confidence << endl;
		cout << "Pattern Orientation: " << orientation << endl;
		rotationMatrix(rotVec, rotMat);
		cout << "Exterior Matrix (from pattern to camera): " << endl;
		for (int i = 0; i < 3; i++) {
			cout << rotMat.at<float>(i, 0) << "\t" << rotMat.at<float>(i, 1) << "\t" << rotMat.at<float>(i, 2) << " |\t" << transVec.at<float>(i, 0) << endl;
		}
	}

	void Pattern::getExtrinsics(int patternSize, const Mat& cameraMatrix, const Mat& distortions)
	{

		CvMat objectPts;//header for 3D points of pat3Dpts
		CvMat imagePts;//header for 2D image points of pat2Dpts
		CvMat intrinsics = cameraMatrix;
		CvMat distCoeff = distortions;
		CvMat rot = rotVec;
		CvMat tra = transVec;
		//		CvMat rotationMatrix = rotMat; // projectionMatrix = [rotMat tra];

		CvPoint2D32f pat2DPts[4];
		for (int i = 0; i < 4; i++) {
			pat2DPts[i].x = this->vertices.at(i).x;
			pat2DPts[i].y = this->vertices.at(i).y;
		}

		//3D points in pattern coordinate system
		CvPoint3D32f pat3DPts[4];
		pat3DPts[0].x = 0.0;
		pat3DPts[0].y = 0.0;
		pat3DPts[0].z = 0.0;
		pat3DPts[1].x = patternSize;
		pat3DPts[1].y = 0.0;
		pat3DPts[1].z = 0.0;
		pat3DPts[2].x = patternSize;
		pat3DPts[2].y = patternSize;
		pat3DPts[2].z = 0.0;
		pat3DPts[3].x = 0.0;
		pat3DPts[3].y = patternSize;
		pat3DPts[3].z = 0.0;

		cvInitMatHeader(&objectPts, 4, 3, CV_32FC1, pat3DPts);
		cvInitMatHeader(&imagePts, 4, 2, CV_32FC1, pat2DPts);

		//find extrinsic parameters
		cvFindExtrinsicCameraParams2(&objectPts, &imagePts, &intrinsics, &distCoeff, &rot, &tra);
	}

	void Pattern::draw(Mat& frame, const Mat& camMatrix, const Mat& distMatrix)
	{
		int i, j;
		RNG rng(12345);
		CvScalar color = cvScalar(255, 255, 255);
		Mat modelV, modelI, modelHept;
		std::vector<cv::Point2f> model2ImagePts;
		switch (id) {
		case 1://V
			color = cvScalar(255, 0, 255);
			modelV = (Mat_<float>(12, 3) <<
				0.00 * size, 0.20 * size, -size,
				0.20 * size, 0.20 * size, -size,
				0.50 * size, 0.20 * size, -0.50 * size,
				0.80 * size, 0.20 * size, -size,
				size, 0.20 * size, -size,
				0.50 * size, 0.20 * size, 0.00,
				0.00 * size, 0.40 * size, -size,
				0.20 * size, 0.40 * size, -size,
				0.50 * size, 0.40 * size, -0.50 * size,
				0.80 * size, 0.40 * size, -size,
				size, 0.40 * size, -size,
				0.50 * size, 0.40 * size, 0.00);
			projectPoints(modelV, rotVec, transVec, camMatrix, distMatrix, model2ImagePts);
			for (i = 0, j = 6; i < 5; i++, j++) {
				line(frame, model2ImagePts.at(i), model2ImagePts.at((i + 1)), color, 3);
				line(frame, model2ImagePts.at(j), model2ImagePts.at((j + 1)), color, 3);
				line(frame, model2ImagePts.at(i), model2ImagePts.at((j)), color, 3);
			}
			line(frame, model2ImagePts.at(11), model2ImagePts.at(6), color, 3);
			line(frame, model2ImagePts.at(5), model2ImagePts.at(0), color, 3);
			line(frame, model2ImagePts.at(5), model2ImagePts.at(11), color, 3);
			break;
		case 2://I
			color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			modelI = (Mat_<float>(24, 3) <<
				0.00 * size, 0.40 * size, 1.00 * -size,
				1.00 * size, 0.40 * size, 1.00 * -size,
				1.00 * size, 0.60 * size, 1.00 * -size,
				0.00 * size, 0.60 * size, 1.00 * -size,
				0.00 * size, 0.40 * size, 0.80 * -size,
				0.40 * size, 0.40 * size, 0.80 * -size,
				0.60 * size, 0.40 * size, 0.80 * -size,
				1.00 * size, 0.40 * size, 0.80 * -size,
				1.00 * size, 0.60 * size, 0.80 * -size,
				0.60 * size, 0.60 * size, 0.80 * -size,
				0.40 * size, 0.60 * size, 0.80 * -size,
				0.00 * size, 0.60 * size, 0.80 * -size,
				0.00 * size, 0.40 * size, 0.20 * size,
				1.00 * size, 0.40 * size, 0.20 * size,
				1.00 * size, 0.60 * size, 0.20 * size,
				0.00 * size, 0.60 * size, 0.20 * size,
				0.00 * size, 0.40 * size, 0.00,
				0.40 * size, 0.40 * size, 0.00,
				0.60 * size, 0.40 * size, 0.00,
				1.00 * size, 0.40 * size, 0.00,
				1.00 * size, 0.60 * size, 0.00,
				0.60 * size, 0.60 * size, 0.00,
				0.40 * size, 0.60 * size, 0.00,
				0.00 * size, 0.60 * size, 0.00
				);
			projectPoints(modelI, rotVec, transVec, camMatrix, distMatrix, model2ImagePts);

			for (i = 0; i < 4; i++)
				line(frame, model2ImagePts.at(i % 4), model2ImagePts.at((i + 1) % 4), color, 3);
			for (i = 12; i < 15; i++)
				line(frame, model2ImagePts.at(i), model2ImagePts.at(i + 1), color, 3);
			line(frame, model2ImagePts.at(0), model2ImagePts.at(4), color, 3);
			line(frame, model2ImagePts.at(1), model2ImagePts.at(7), color, 3);
			line(frame, model2ImagePts.at(10), model2ImagePts.at(11), color, 3);
			line(frame, model2ImagePts.at(10), model2ImagePts.at(22), color, 3);
			line(frame, model2ImagePts.at(12), model2ImagePts.at(15), color, 3);
			line(frame, model2ImagePts.at(12), model2ImagePts.at(16), color, 3);
			line(frame, model2ImagePts.at(13), model2ImagePts.at(19), color, 3);
			line(frame, model2ImagePts.at(14), model2ImagePts.at(20), color, 3);
			line(frame, model2ImagePts.at(16), model2ImagePts.at(17), color, 3);
			line(frame, model2ImagePts.at(16), model2ImagePts.at(23), color, 3);
			line(frame, model2ImagePts.at(17), model2ImagePts.at(5), color, 3);
			line(frame, model2ImagePts.at(18), model2ImagePts.at(19), color, 3);
			line(frame, model2ImagePts.at(18), model2ImagePts.at(21), color, 3);
			line(frame, model2ImagePts.at(19), model2ImagePts.at(20), color, 3);
			line(frame, model2ImagePts.at(2), model2ImagePts.at(8), color, 3);
			line(frame, model2ImagePts.at(20), model2ImagePts.at(21), color, 3);
			line(frame, model2ImagePts.at(22), model2ImagePts.at(23), color, 3);
			line(frame, model2ImagePts.at(23), model2ImagePts.at(15), color, 3);
			line(frame, model2ImagePts.at(3), model2ImagePts.at(11), color, 3);
			line(frame, model2ImagePts.at(4), model2ImagePts.at(11), color, 3);
			line(frame, model2ImagePts.at(4), model2ImagePts.at(5), color, 3);
			line(frame, model2ImagePts.at(5), model2ImagePts.at(10), color, 3);
			line(frame, model2ImagePts.at(6), model2ImagePts.at(18), color, 3);
			line(frame, model2ImagePts.at(6), model2ImagePts.at(7), color, 3);
			line(frame, model2ImagePts.at(6), model2ImagePts.at(9), color, 3);
			line(frame, model2ImagePts.at(7), model2ImagePts.at(8), color, 3);
			line(frame, model2ImagePts.at(8), model2ImagePts.at(9), color, 3);
			line(frame, model2ImagePts.at(9), model2ImagePts.at(21), color, 3);
			break;
		case 3://heptagon
			color = cvScalar(0, 255, 255);
			modelHept = (Mat_<float>(14, 3) <<
				0.53 * size, 0.00 * size, -size,
				0.90 * size, 0.18 * size, -size,
				0.99 * size, 0.57 * size, -size,
				0.74 * size, 0.88 * size, -size,
				0.33 * size, 0.88 * size, -size,
				0.08 * size, 0.57 * size, -size,
				0.17 * size, 0.18 * size, -size,
				0.53 * size, 0.00 * size, 0,
				0.90 * size, 0.18 * size, 0,
				0.99 * size, 0.57 * size, 0,
				0.74 * size, 0.88 * size, 0,
				0.33 * size, 0.88 * size, 0,
				0.08 * size, 0.57 * size, 0,
				0.17 * size, 0.18 * size, 0
				);
			projectPoints(modelHept, rotVec, transVec, camMatrix, distMatrix, model2ImagePts);
			for (i = 0; i < 7; i++)
			{
				line(frame, model2ImagePts.at(i), model2ImagePts.at((i + 1) % 7), color, 3);
				line(frame, model2ImagePts.at(i + 7), model2ImagePts.at(((i + 1) % 7) + 7), color, 3);
				line(frame, model2ImagePts.at(i), model2ImagePts.at(i + 7), color, 3);
			}
			break;
		}
		model2ImagePts.clear();

	}

}