#include "Functions.h"

int main() {

	char* source_windowA = "Source imageA";



	Mat F = imread("Filter.jpg", 0);

	if (F.empty()) return -1;

	F = MeanFilter(F);
	F = mean(F);

	namedWindow(source_windowA, CV_WINDOW_KEEPRATIO);



	imshow(source_windowA, F);



	waitKey(0);
	return 0;
}
