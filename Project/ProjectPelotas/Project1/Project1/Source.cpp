#include "opencv2/core/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

Mat src; Mat src_gray;
int cannyThres = 100;
int imageThres = 67;
int max_thresh = 255;
int minArea = 400, maxArea = 250000;
RNG rng(12345);
const float PI = 3.1415926535;
/// Function header
void CannyCalculation();
void emptyCallback(int, void*);

/**
* @function main
*/
int main(int, char** argv)
{
	int erosion_size = 1;
	const char* source_window = "Source";
	const char* bin_window = "bin";

	VideoCapture cap(1);
	if (!cap.isOpened())
		return -1;

	namedWindow(source_window, WINDOW_AUTOSIZE);
	namedWindow(bin_window, WINDOW_AUTOSIZE);
	namedWindow("trackbars", WINDOW_AUTOSIZE);
	//trackbars for area minima, maxima y otras utilidades
	createTrackbar("Min area:", "trackbars", &minArea, maxArea, emptyCallback);
	createTrackbar("Max area:", "trackbars", &maxArea, 250000, emptyCallback);
	createTrackbar("thres:", "trackbars", &imageThres, 255, emptyCallback);
	int key = 0;

	Mat element = getStructuringElement(MORPH_CROSS,
		Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		Point(erosion_size, erosion_size));

	while (key != 'q') {
		cap >> src;
		/// Convert image to gray and blur it
		cvtColor(src, src_gray, COLOR_BGR2GRAY);
		threshold(src_gray, src_gray, imageThres, 255, CV_THRESH_BINARY);
		erode(src_gray, src_gray, element);
		dilate(src_gray, src_gray, element);
		blur(src_gray, src_gray, Size(3, 3));

		CannyCalculation();

		/// Create Window
		imshow(source_window, src);
		imshow(bin_window, src_gray);

		key = waitKey(20);
	}
	return (0);
}

/**
* @function thresh_callback
*/
void emptyCallback(int, void*) {
	if (minArea < 400) minArea = 400;
}
void CannyCalculation()
{
	Mat canny_output;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	/// Detect edges using canny
	Canny(src_gray, canny_output, cannyThres, cannyThres * 2, 3);
	/// Find contours
	findContours(canny_output, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));

	/// Get the moments
	vector<Moments> mu(contours.size());
	for (size_t i = 0; i < contours.size(); i++)
	{
		mu[i] = moments(contours[i], false);
	}

	///  Get the mass centers:
	vector<Point2f> mc(contours.size());
	for (size_t i = 0; i < contours.size(); i++)
	{
		mc[i] = Point2f(static_cast<float>(mu[i].m10 / mu[i].m00), static_cast<float>(mu[i].m01 / mu[i].m00));
	}

	/// Draw contours
	Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);
	for (size_t i = 0; i < contours.size(); i++)
	{
		//Check how circular is something
		//C=(4PI*A)/P^2
		double area = contourArea(contours.at(i));
		if (minArea < area && area < maxArea) {
			Scalar color = Scalar(255, 255, 255);
			float circularity = (4 * PI * area) / pow(arcLength(contours.at(i), true), 2);
			if (circularity >= 0.85f) {
				circle(drawing, mc[i], 4, color, -1, 8, 0);
				drawContours(drawing, contours, (int)i, color, 2, 8, hierarchy, 0, Point());
			}
			//Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		}
	}
	//add the drawing to src
	drawing.inv();
	src = src + drawing;
	/// Show in a window
	namedWindow("Contours", WINDOW_AUTOSIZE);
	imshow("Contours", drawing);
}
